--[[
    使用概率限流

    Author: mahaixing@gmail.com
    License: MIT
]]

local assert = assert
local math = require "math"
local utils = require "utils"

local _M = require("limiters.limiter"):new()

function _M:process_config()
    self.probability_rate = assert(self.conf.probability_rate, 
                                "Please set probability_rate in probability section!")
    self.probability_rate = tonumber(self.probability_rate)
    if self.probability_rate == nil then
        utils.log("Probability rate must a number!")
        error("Probability rate must a number!")
    else 
        self.probability_rate = self.probability_rate * 100
    end
end

function _M:execute()

    math.randomseed(os.time())
	--判断随机值是否在限行概率范围内 
    if (math.random(1,10000) <= self.probability_rate) then
        return true 
    else
        return false
    end
end

return _M